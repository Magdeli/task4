﻿using System;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            // Magdeli Holmøy Asplin
            // 8/15/2019

            // This program takes input from user on the size of a rectangle and uses this to return 
            // a rectangle with another rectangle inside.

            #region Variables
            // In this region all the variables used are declared and some are assigned values.

            int n;
            int m;
            string square = "#";
            string space = " ";

            #endregion


            #region Getting the size of the rectangle
            // In this region the input is collected, and the program will not continue unless the 
            // given input is an integer.

            do
            {
                Console.WriteLine("Please enter a number for how large you want two sides of the rectangle to be:");

                n = int.Parse(Console.ReadLine());

            } while (n % 1 != 0);

            do
            {
                Console.WriteLine("Please enter a number for how large you want the other sides of the rectangle to be:");

                m = int.Parse(Console.ReadLine());

            } while (m % 1 != 0);

            #endregion

            #region Making the rectangle
            // In this region the rectangles are made and returned.

            for (int i = 1; i < n; i++)
            {
                if (i == 1 || i == (n-1))
                {
                    // This if-loop creates the top and bottom of the inner rectangle.

                    for (int j = 0; j < m; j++)
                    {
                        Console.Write(square);
                    }
                    Console.WriteLine();
                    continue;
                }

                if (i == 2 || i == (n-2))
                {
                    // This if-loop creates the second and second last lines of the figure.

                    for (int j = 0; j < m; j++)
                    {
                        if (j == 0 || j == (m-1))
                        {
                            Console.Write(square);
                        }
                        else
                        {
                            Console.Write(space);
                        }
                    }
                    Console.WriteLine();
                    continue;
                }

                if (i == 3 || i == (n-3))
                {
                    // This if-loop creates the two lines which contains the upper and lower sides 
                    // of the inner square.

                    for (int j = 0; j < m; j++)
                    {
                        if (j == 1 || j == (m-2))
                        {
                            Console.Write(space);
                        }
                        else
                        {
                            Console.Write(square);
                        }
                    }
                    Console.WriteLine();
                    continue;
                }

                else
                {
                    // This loop creates the rest of the figure, which is the lines containing 
                    // sides of both the rectangles.

                    for (int j = 0; j < m; j++)
                    {
                        if (j == 0 || j == 2 || j == (m-3) || j == (m-1))
                        {
                            Console.Write(square);
                        }
                        else
                        {
                            Console.Write(space);
                        }
                    }
                    Console.WriteLine();
                }
            }

            #endregion
        }
    }
}
